# pynextage
python scripts to control nextage

The following libraries are used

1. Panda3D
2. Networkx
3. Numpy, Scipy
4. MySQLdb

The following executable programs are used:

1. MySQL set (including server, bench, dump, etc.)
    * Import *.sql as into the DBMS system first.

The program follows these common sense:

1. A 3d point or nd vector is represented using one row of np.array list
2. A n-by-3 matrix is represented by n rows of np.array list

Executing the program for HRP5P

1. Compute the grasps using freegrip.py (under manipulation/grip)
2. Compute the stable placements using freetabletopplacement.py (under manipulation/grip)
3. Compute the stable placements and ik-feasible grasps all over a table surface using tableplacements.py (under manipulation/regrasp)
4. Build the regrasp graph using regriptpp.py (under manipulation/regrasp)
Step 4 is integrated in regrasp/hrp5plot.py, see the final results by executing this file.

Executing the program for Dual-arm assembly

1. Compute the grasps using freegrip.py (under manipulation/grip)
2. Compute the stable placements using freetabletopplacement.py (under manipulation/grip)
3. Compute the stable placements and ik-feasible grasps all over a table surface using tableplacements.py (under manipulation/regrasp)
4. Execute manipulation/assembly/asstwoobj.py. The script saves/loads assembly, generates assemblyx,
assemblyxgrippairs, assemblyxgripsp0, and assemblyxgrips1. It also updates the ikassemblygrips0 and
ikassemblygrips1. Assembly (relative poses of two objects), Assemblyx: Assembly with rotation.
5. Execute nxttppassplot.py to find one plan

Delete the grasp data (in MySQL workbench):
```sql
SET FOREIGN_KEY_CHECKS=0;
truncate table nextagegrip.tabletopplacements;
truncate table nextagegrip.tabletopgrips;
truncate table nextagegrip.ik;
truncate table nextagegrip.freeairgrip;
truncate table nextagegrip.freetabletopplacement;
truncate table nextagegrip.freetabletopgrip;
SET FOREIGN_KEY_CHECKS=1;
```

Delete the tabletop regrasp data (in MySQL workbench):
```sql
SET FOREIGN_KEY_CHECKS=0;
truncate table nextagegrip.tabletopplacements;
truncate table nextagegrip.tabletopgrips;
truncate table nextagegrip.ik;
SET FOREIGN_KEY_CHECKS=1;
```

Delete the floating pose data (in MySQL workbench):
```sql
SET FOREIGN_KEY_CHECKS=0;
truncate table nextagegrip.floatingposes;
truncate table nextagegrip.floatinggrips;
truncate table nextagegrip.floatinggripspairs;
truncate table nextagegrip.ikfloatinggrips;
SET FOREIGN_KEY_CHECKS=1;
```

Delete the dual-arm assembly data (in MySQL workbench):
```sql
SET FOREIGN_KEY_CHECKS=0;
truncate table nextagegrip.assembly;
truncate table nextagegrip.assemblyx;
truncate table nextagegrip.assemblyxgrippairs;
truncate table nextagegrip.assemblyxgrips0;
truncate table nextagegrip.assemblyxgrips1;
truncate table nextagegrip.ikassemblyxgrips0;
truncate table nextagegrip.ikassemblyxgrips1;
SET FOREIGN_KEY_CHECKS=1;
```